import json
import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_photo(city, state):
    headers = {"Authorization": PEXELS_API_KEY}
    params = {
        "per_page": 1,
        "query": f"{city} {state}"
    }
    url = "https://api.pexels.com/v1/search"
    response = requests.get(url, params=params, headers=headers)
    content = json.loads(response.content)
    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except (KeyError, IndexError):
        return {"picture_url": None}


def get_weather_data(city, state):
    appid = OPEN_WEATHER_API_KEY
    # params = {
    #     "q": f"{city},{state}",
    #     "appid": f"{OPEN_WEATHER_API_KEY}",
    # }
    url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},US&appid={appid}"
    print(url)
    datacall = requests.get(url)
    print(datacall)
    cont = json.loads(datacall.content)
    print(cont)
    lat = cont[0]["lat"]
    lon = cont[0]["lon"]
    print(lat, lon)
    apicall = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&units=imperial&appid={appid}"
    response = requests.get(apicall)
    content = json.loads(response.content)
    description = content["weather"][0]["description"]
    temp = content["main"]["temp"]
    try:
        return {"weather": {
            "description": description,
            "temp": temp,
            }}
    except (KeyError, IndexError):
        return {"weather": None}
